from rest_framework.response import Response
import jwt, json
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import get_authorization_header, BaseAuthentication
from rest_framework import status, exceptions
from rest_framework.permissions import BasePermission
from django.contrib.auth.models import AnonymousUser
from database.geomodels import Ambulances
from database.models import *
from database.DBApi import db_connection
from Clinic.Profile_Views import TokenAuthentication, JAuthentificated
from database.login import *
import datetime

NEAR_AMBULANCES = 20000


class ApointmentView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [JAuthentificated]

    def post(self, request):

        type_of_user = User.get_type_by_id(request.user.id, db_connection)
        try:
            time_slot = int(request.data['time_slot'])
            if time_slot < 0 or time_slot > 18:
                raise ValueError
            day = request.data['date']
        except:
            return Response(status=400,
                            data={'Error': 'Incorrect time slot'})

        doc_id = None
        pat_id = None

        if type_of_user is 'doctor':
            if 'doctor' in request.data and request.data['doctor'] != request.user.id:
                return Response(status=400,
                                data={'Error': 'Doctor can not create appointments for other doctors'})
            doc_id = request.user.id
            pat_id = request.data['patient']

        elif type_of_user is 'patient':
            if 'patient' in request.data and request.data['patient'] != request.user.id:
                return Response(status=400,
                                data={'Error': 'Patient can not create appointments for other patients'})
            pat_id = request.user.id
            doc_id = request.data['doctor']

        elif type_of_user is 'receptionist':
            try:
                pat_id = request.data['patient']
                doc_id = request.data['doctor']
            except:
                return Response(status=400, data={'Error': 'Provide values'})
        else:
            return Response(status=400, data={'Error': 'No access'})

        if pat_id is None or doc_id is None:
            return Response(status=400, data={'Error': 'Can not create appointment'})

        doctor = Doctor.get_by_id(doc_id, db_connection)
        if doctor is None or len([app for app in doctor.get_appointments(db_connection) if
                                  app.date == day and app.time_slot == time_slot]) > 0:
            return Response(status=400, data={'Error': 'Doctor not available'})

        patient = Patient.get_by_id(pat_id, db_connection)
        if patient is None or len([app for app in patient.get_records(db_connection) if
                                   app.date == day and app.time_slot == time_slot]) > 0:
            return Response(status=400, data={'Error': 'Patient not available'})
        creation_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        app = Appointment(date=day, time_slot=time_slot, patient=patient, doctor=doctor, creation_date=creation_date)

        if app.save(db_connection) is None:
            return Response(status=400, data={'Error': 'Can not create appointment on this slot'})
        else:
            return Response(status=200, data='Appointment created')

    def get(self, request):
        appointments = []
        type_of_user = request.user.type
        if type_of_user == "doctor":
            appointments = request.user.get_appointments(db_connection)
        elif type_of_user == "patient":
            appointments = request.user.get_records(db_connection)
        elif type_of_user == "receptionist":
            appointments = Appointment.get_all(db_connection)
        else:
            return Response(status=400, data={'Error': 'No access'})
        return Response(status=200, data=[
            {'doctor': app.doctor.id,
             'patient': app.patient.id, 'time': app.time, 'date': app.date, 'creation_date': app.creation_date} for
            app in appointments
        ])


class DoctorsView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [JAuthentificated]

    def get(self, request):
        doctors = Doctor.get_all(db_connection)
        return Response(status=200, data={
            doctor.id: doctor.first_name + " " + doctor.last_name for
            doctor in doctors})


class PatientsView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [JAuthentificated]

    def get(self, request):
        patients = Patient.get_all(db_connection)
        return Response(status=200, data={
            patient.id: patient.first_name + " " + patient.last_name for
            patient in patients})


class AmbulancesView(APIView):

    def post(self, request):
        lat = request.query_params['lat']
        long = request.query_params['long']
        ambulances = Ambulances('ambulances').get_nearby(long=long, lat=lat, rad_meters=NEAR_AMBULANCES,
                                                         con=db_connection)

        ambulances = sorted(ambulances, key=lambda x: x[1])
        return Response(status=200,
                        data=[{'place': a[0], 'distance': a[1], 'long': a[2][0], 'lat': a[2][1]} for a in ambulances])
