from django.contrib import admin
from django.urls import path, include
from Clinic.Profile_Views import LoginView, ProfileView, RegView, ChangePasswordView
from Clinic.Clinic_Views import ApointmentView, DoctorsView, AmbulancesView, PatientsView

urlpatterns = [
    path('login/', LoginView.as_view()),
    path('profile/', ProfileView.as_view()),
    path('register/', RegView.as_view()),
    path('doctors/', DoctorsView.as_view()),
    path('appointments/', ApointmentView.as_view()),
    path('password/', ChangePasswordView.as_view()),
    path('ambulances/', AmbulancesView.as_view()),
    path('patients/', PatientsView.as_view())

]
