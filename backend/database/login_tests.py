import unittest
from backend.database.models import User
from backend.database.DBApi import db_connection
from backend.database.login import registration, login


class MyTestCase(unittest.TestCase):
    def test_login(self):

        con = db_connection
        con.client.flushall()
        emp = registration('lolkek', '123', 20, 'Male', '', 'adssa', 'asdsad', con)
        usernew = login('lolkek', '123', con)
        emp_fetched = User.get_user_with_correct_type('lolkek', con)

        print(emp.__dict__)
        print(usernew.__dict__)
        print(emp_fetched.__dict__)

        self.assertTrue(emp.__dict__ == usernew.__dict__ == emp_fetched.__dict__)
        self.assertEqual(usernew.key, emp.key)
        self.assertEqual(usernew.type, emp.type)


if __name__ == '__main__':
    unittest.main()
