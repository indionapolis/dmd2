import unittest
from database.models import *
from database.DBApi import Connection
from database.geomodels import Ambulances

port = '6379'


class DBTests(unittest.TestCase):

    def test_repr(self):

        r = Room(number='123', room_type='hospital room')
        p = Patient(username="pat1", room=r)
        d = Doctor(username='doctor_kek', first_name='Oleg', last_name='Popov')
        a = Appointment('super appointment', p, d)

        print(a)
        print(d)
        print(p)
        print(r)

    def test_user_type_changing(self):
        con = Connection(port=port)
        con.client.flushall()

        u = User('1')
        u = u.make_employee(con, salary=7000)

        u2 = Employee.get_by_id('1', con)
        self.assertTrue(u.salary == u2.salary)
        self.assertTrue(u.__dict__ == u2.__dict__)

    def test_room_nurse_relation(self):
        con = Connection(port=port)
        con.client.flushall()

        r1 = Room(number='1', room_type='hospital room')
        r2 = Room(number='2', room_type='doctor room')
        r3 = Room(number='3', room_type='hospital room')
        r4 = Room(number='4', room_type='hospital room')

        r1.save(con)
        r2.save(con)
        r3.save(con)
        r4.save(con)

        n1 = Nurse(username='nurse1@nursemail.com')
        n2 = Nurse(username='nurse2@nursemail.com')
        n3 = Nurse(username='nurse3@nursemail.com')
        n4 = Nurse(username='nurse4@nursemail.com')

        n1.save(con)
        n2.save(con)
        n3.save(con)
        n4.save(con)

        n1.add_room('maintain breathing technology', room=r1, con=con)
        n1.add_room('maintain heartscan technology', room=r2, con=con)  # Same room
        n1.add_room('maintain children toys', room=r2, con=con)  # Same room

        initial_rooms = [r1.key, r2.key]

        fetched_rooms = []
        rooms = n1.get_rooms(con)
        for room in rooms:
            fetched_rooms.append(room.key)

        print(fetched_rooms, ' == ', initial_rooms)
        self.assertTrue(compare(initial_rooms, fetched_rooms))

        n1.del_room(r1, con)
        n1.del_room(r2, con)

        initial_rooms = []
        fetched_rooms = []
        rooms = n1.get_rooms(con)
        for room in rooms:
            fetched_rooms.append(room.key)

        self.assertTrue(initial_rooms == fetched_rooms == [])

        # self.assertTrue(len(con.client.keys('*')) == 0)

    def test_patient_redefine(self):

        con = Connection(port=port)
        con.client.flushall()

        p = Patient(username='1', password='asdjasdlaskdjla')
        p.save(con)

        p2 = Patient.get_by_id('1', con)
        p2.password = '12345678'
        p2.redefine(con)

        p2 = Patient.get_by_id('1', con)

        self.assertTrue(p2.password == '12345678')

    def test_patient_room_indexing(self):
        con = Connection(port=port)
        con.client.flushall()

        r = Room(number='123', room_type='hospital room')

        init_patients = []

        p = Patient(username="pat1", room=r)
        p.save(con)
        init_patients.append(p.id)
        p = Patient(username="pat2", room=r)
        p.save(con)
        init_patients.append(p.id)
        p = Patient(username="pat3", room=r)
        p.save(con)
        init_patients.append(p.id)

        fetched_patients = []
        patients = r.get_patients(con)
        for patient in patients:
            fetched_patients.append(patient.id)

        self.assertTrue(compare(init_patients, fetched_patients))

    def test_indexing(self):

        con = Connection(port=port)
        con.client.flushall()

        p = Patient(username="111")
        r = Receptionist(username='222')
        d = Doctor(username='DoctorWho')
        p.save(con)
        r.save(con)
        d.save(con)

        # Create Records and push them in Redis. all linked to, 'p', 'd'
        appointments_list = []
        for i in range(10):
            created_entity = Appointment(description='desc #' + str(i), patient=p, doctor=d)
            if created_entity.save(con):
                appointments_list.append(created_entity.key)

        print(con.client.keys('*'))

        # Get list of pushed Records from Redis
        db_list = []
        for rec in d.get_appointments(con):
            db_list.append(rec.key)

        # Print lists to see why we need to sort them in compare
        print(appointments_list)
        print(db_list)

        # Test whether initial entities' key and fetched entities' key are the same)
        self.assertTrue(compare(appointments_list, db_list))

        print('all appointments: ', Appointment.get_all(con))

        # Delete all created entities from redis and (if current removal is successful) delete from initial list
        entities = Appointment.get_all(con)
        for entity in entities:
            if entity.delete(con):
                appointments_list.remove(entity.key)

        # fetch all RELATED to receptionist Records (there must be no records because we deleted them)
        db_list = []
        for rec in d.get_appointments(con):
            db_list.append(rec.key)

        # check whether there are no records in initial list and in fetched (via receptionist) records
        self.assertTrue([] == appointments_list == db_list)

        p.delete(con)
        r.delete(con)
        d.delete(con)
        print(con.client.keys('*'))

    def test_type_checking(self):

        con = Connection(port=port)
        con.client.flushall()

        e = Employee('employee@example.com')
        d = Doctor('doctor@example.com')
        n = Nurse('nurse@example.com')
        u = User('user@example.com')
        p = Patient('patient@example.com')

        e.save(con)
        d.save(con)
        n.save(con)
        u.save(con)
        p.save(con)

        print(User.get_type_by_id(e.id, con=con))
        print(User.get_type_by_id(d.id, con=con))
        print(User.get_type_by_id(n.id, con=con))
        print(User.get_type_by_id(u.id, con=con))
        print(User.get_type_by_id(p.id, con=con))

        self.assertTrue(User.get_type_by_id(e.id, con=con) is 'employee')
        self.assertTrue(User.get_type_by_id(d.id, con=con) is 'doctor')
        self.assertTrue(User.get_type_by_id(n.id, con=con) is 'nurse')
        self.assertTrue(User.get_type_by_id(u.id, con=con) is 'user')
        self.assertTrue(User.get_type_by_id(p.id, con=con) is 'patient')

    # Check that we can not save
    # 'patient:same@sameemail.com' and
    # 'doctor:same@sameemail.com'
    def test_users_id_uniqueness(self):
        con = Connection(port=port)

        p = Patient('lolkek')
        e = Employee('lolkek')
        p.save(con)
        result = e.save(con)

        self.assertTrue(result is False)

        pf = Patient.get_by_id('lolkek', con)
        ef = Employee.get_by_id('lolkek', con)

        self.assertTrue(pf.id == 'lolkek')
        self.assertTrue(ef is None)

    def test_appointments(self):
        con = Connection(port=port)
        con.client.flushall()

        doc = Doctor('doctor@mail.rf')
        doc.save(con)
        pat = Patient('patient@mail.ru')
        pat.save(con)

        app = Appointment('blabla', pat, doc)
        app.save(con)

        app_fetched = Appointment.get_by_id(app.id, con)

        print(app.__dict__)
        print(app_fetched.__dict__)

        d1 = app.__dict__.copy()
        del d1['doctor']
        del d1['patient']

        d2 = app.__dict__.copy()
        del d2['doctor']
        del d2['patient']

        self.assertTrue(d1 == d2)

    def test_appointments_date(self):
        con = Connection(port=port)
        con.client.flushall()

        doc = Doctor('doctor@mail.rf')
        doc.save(con)
        pat = Patient('patient@mail.ru')
        pat.save(con)

        app = Appointment('blabla', pat, doc, time=75000, date='2019-04-12', time_slot=3)
        app.save(con)

        app_fetched = Appointment.get_by_id(app.id, con)

        print(Appointment('blabla', pat, doc, time_slot=3).__dict__)

        self.assertTrue(app_fetched.doctor.key == app.doctor.key)
        self.assertTrue(app_fetched.doctor.id == app.doctor.id)
        self.assertTrue(app_fetched.time == app.time)


class DBGEOTests(unittest.TestCase):

    def test_add_edit_delete(self):
        con = Connection(port=port)
        con.client.flushall()

        ambulances = Ambulances('ambulances')

        ambulances.add('Парковая/Центральная Улица', 48.74928832, 55.74326247, con)
        ambulances.add('Парковая/Спортивная  Улица', 48.73936415, 55.74706704, con)
        ambulances.add('Цунтральный перекресток', 48.74455690, 55.75211506, con)
        ambulances.add('Центр', 48.74726057, 55.7482023, con)

        print(ambulances.get_nearby(long=48.74726057, lat=55.7482023, con=con, rad_meters=500))


def compare(s, t):
    return sorted(s) == sorted(t)


if __name__ == '__main__':
    unittest.main()
