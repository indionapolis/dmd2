import redis
from redis.exceptions import ConnectionError
import os


"""Redis Connection abstraction to pass as arg in API"""
class Connection:
    db = os.environ.get('DATA_BASE')

    def __init__(self, host=db if db else 'localhost', port='6379'):
        try:
            self.client = redis.Redis(host=host, port=port, decode_responses=True)
            self.client.set('test', 'testing')
            self.client.delete('test')
            self.connected = True
        except ConnectionError:
            self.connected = False
            print('Redis Server is unreachable')


db_connection = Connection()


class DBEntity:
    """This class define an abstraction to work with object with redis.

    To make a model which represents an object that can be stored, fetched etc in REDIS,
    you need to inherit a class and define fields. DB methods as save()
    iterate through all fields of a class and save them as 'hash' in REDIS
    You can find examples in models.py

    Args:
        dbid (string): Part of the REDIS key.
        The key itself is type:id (needs for patterns support)
    """
    def __init__(self, dbid: str):
        self.type = self.__class__.__name__.lower()
        self.id = dbid
        self.key = self.type + ':' + self.id

    def save(self, con: Connection):
        """Method which save our class instance in REDIS

            Args:
                con (Connection): REDIS client.

            It checks object existence, and then iterate through each
            field of the class to save result as REDIS 'hash'.
            If the field is str: it save as str,
            if it is int or bool: converts to string,
            if it is ForeignKey (key to other REDIS obj): it save only key as field
            (but not object 'foreign key')
            and also create indexing using redis SET

            For example Author1 -> Book3, Author2 -> Book3
            (author instances has same book foreign key)
            It will save Author keys in SET:
            one_to_many:book:3:author -> {author:1, author:2}
            i.e we have <one_to_many> relation:
            <book:3> has <author>s with keys: {author:1, author:2}
            """
        if not con.client.exists(self.key):

            class_fields = self.__dict__.copy()
            del class_fields['key']

            # Foreign keys, int, bool type handling
            for field_key in class_fields:
                field_value = class_fields[field_key]
                if isinstance(field_value, ForeignKey):
                    con.client.sadd('one_to_many:' + field_value.key + ':' + self.type, self.key)
                    class_fields[field_key] = field_value.key

                if isinstance(field_value, (int, bool)):
                    class_fields[field_key] = str(class_fields[field_key])

            con.client.hmset(self.key, class_fields)

            return True
        else:
            return False

    def redefine(self, con: Connection):
        """If we need to change obj fields after GET we change
             it in instance of a class and then redefine REDIS object"""
        self.delete(con)
        self.save(con)
        return True

    @classmethod
    def get_by_key(cls, key, con):
        """Getter for object using key (for internal API work)
            Work same as Getter by id"""
        id = key.split(':')[1]
        if con.client.exists(key):

            entity = cls(id)

            class_fields = entity.__dict__
            db_entity = con.client.hgetall(key)
            class_fields['key'] = key
            class_fields['id'] = id

            # Push values from db into entity
            for key in db_entity:
                class_field_value = class_fields[key]
                if isinstance(class_field_value, ForeignKey):
                    class_fields[key] = ForeignKey(db_entity[key])
                elif isinstance(class_field_value, int):
                    class_fields[key] = int(db_entity[key])
                elif isinstance(class_field_value, bool):
                    class_fields[key] = bool(db_entity[key])
                else:
                    class_fields[key] = db_entity[key]

            return entity
        else:
            return None

    @classmethod
    def get_by_id(cls, dbid, con):
        """Getter for object which use id
            It assemble python class instance using redis data.
            To get an object we check whether it exists,
            create an empty instance of a class which we call,
            fetch data from redis and then write redis data into
            class instance using same field names
            (Instance field is a dict (not immutable), REDIS data is also a dict
            dict fields has the same key names).
            It also restore int, bool, Foreign key types by checking initial
            dummy object (cls()) field types

            Args:
                con (Connection): REDIS client.
                dbid (str): id of an object (not key)
            """
        key = cls.__name__.lower() + ':' + dbid
        if con.client.exists(key):

            entity = cls(dbid)

            class_fields = entity.__dict__
            db_entity = con.client.hgetall(key)
            class_fields['key'] = key

            # Push values from db into entity
            for key in db_entity:
                class_field_value = class_fields[key]
                if isinstance(class_field_value, ForeignKey):
                    class_fields[key] = ForeignKey(db_entity[key])
                elif isinstance(class_field_value, int):
                    class_fields[key] = int(db_entity[key])
                elif isinstance(class_field_value, bool):
                    class_fields[key] = bool(db_entity[key])
                else:
                    class_fields[key] = db_entity[key]

            return entity
        else:
            return None

    def delete(self, connection: Connection):
        """Removes object from redis. Also clean the indexing"""
        if connection.client.delete(self.key) != 0:

            class_fields = self.__dict__.copy()

            # Foreign keys, int, bool type handling
            for field_key in class_fields:
                field_value = class_fields[field_key]
                if isinstance(field_value, ForeignKey):
                    connection.client.srem('one_to_many:' + field_value.key + ':' + self.type, self.key)

            return True
        else:
            return False

    @classmethod
    def get_all(cls, connection):
        """Return list of all object in REDIS of such class type.
        Utilize the way I implement a keys <Type:id> using REDIS patterns
        For example key pattern <Car:*> will return all cars keys"""
        entity_list = []
        all_keys = connection.client.keys(cls.__name__.lower() + ':*')
        for entity_key in all_keys:
            entity = cls.get_by_key(entity_key, connection)
            entity_list.append(entity)
        return entity_list

    def __repr__(self):
        class_fields = self.__dict__.copy()
        del class_fields['key']

        # Foreign keys, int, bool type handling
        for field_key in class_fields:
            field_value = class_fields[field_key]
            if isinstance(field_value, ForeignKey):
                class_fields[field_key] = field_value.key

            if isinstance(field_value, (int, bool)):
                class_fields[field_key] = str(class_fields[field_key])

        return '<' + self.key + '>: ' + str(class_fields)


class ForeignKey:

    def __init__(self, key):
        self.key = key
        self.id = key.split(':')[1]
        self.type = key.split(':')[0]

    def fields(self, con: Connection):
        return con.client.hgetall(self.key)


def get_seconds_from_midnight():
    import datetime
    now = datetime.datetime.now()
    midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
    seconds = (now - midnight).seconds
    return seconds


class GEO_Spatial_SET:
    """Abstraction to work with GEO position in REDIS"""
    def __init__(self, setkey):
        self.setkey = setkey

    def add(self, name: str, long: float, lat: float, con: Connection):
        con.client.geoadd(self.setkey, long, lat, name)

    def update(self, name: str, long: float, lat: float, con: Connection):
        con.client.geoadd(self.setkey, long, lat, name)

    def rem(self, name: str, con: Connection):
        con.client.zrem(self.setkey, name)

    def get_nearby(self, long: float, lat: float, rad_meters: int, con: Connection):
        return con.client.georadius(self.setkey,
                                    longitude=long,
                                    latitude=lat,
                                    radius=rad_meters,
                                    unit='m',
                                    withdist=True,
                                    withcoord=True)
