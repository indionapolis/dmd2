from datetime import datetime, time as time_class
from database.DBApi import DBEntity, ForeignKey, get_seconds_from_midnight, Connection


class Room(DBEntity):
    def __init__(self,
                 number: str = '',
                 room_type: str = 'hospital room',
                 ):
        super().__init__(number)
        self.room_type = room_type

    def get_patients(self, con):
        patients = []
        for r in con.client.smembers('one_to_many:' + self.key + ':' + Patient.__name__.lower()):
            patient = Patient.get_by_key(r, con)
            if patient is not None:
                patients.append(patient)
        return patients

    def get_nurses(self, con: Connection):
        nurses = []
        keys = con.client.keys('nursegovernsroom:*' + '_' + self.id)
        for key in keys:
            m2m = NurseGovernsRoom.get_by_key(key, con)
            nurses.append(Nurse.get_by_key(m2m.nurse_key, con))
        return nurses


class User(DBEntity):
    def __init__(self,
                 username: str,
                 first_name: str = '',
                 last_name: str = '',
                 password: str = '',
                 age: int = 0,
                 sex: str = 'Male',
                 address: str = '',
                 history: str = '',
                 contact: str = ''):
        super().__init__(dbid=username)
        self.first_name = first_name
        self.last_name = last_name
        self.password = password
        self.age = age
        self.address = address
        self.sex = sex
        self.history = history
        self.contact = contact

    def save(self, con: Connection):
        # There is no doctors, patients.. etc
        # (users of different type) with the same id
        a = User.get_type_by_id(self.id, con)
        if a is 'user':
            return super(User, self).save(con)
        else:
            return False

    @staticmethod
    def get_user_with_correct_type(dbid, con):
        user_type = User.get_type_by_id(dbid, con)
        if user_type is 'patient':
            return Patient.get_by_id(dbid, con)
        if user_type is 'employee':
            return Employee.get_by_id(dbid, con)
        if user_type is 'doctor':
            return Doctor.get_by_id(dbid, con)
        if user_type is 'nurse':
            return Nurse.get_by_id(dbid, con)
        if user_type is 'receptionist':
            return Receptionist.get_by_id(dbid, con)

        return User.get_by_id(dbid, con)

    @staticmethod
    def get_type_by_id(uid, con: Connection):
        if Employee.get_by_id(uid, con) is not None:
            return 'employee'
        if Doctor.get_by_id(uid, con) is not None:
            return 'doctor'
        if Patient.get_by_id(uid, con) is not None:
            return 'patient'
        if Nurse.get_by_id(uid, con) is not None:
            return 'nurse'
        if Receptionist.get_by_id(uid, con) is not None:
            return 'receptionist'
        return 'user'

    def make_employee(self, con: Connection, salary: int = 0, ):
        self.delete(con)
        emp = Employee(username=self.id,
                       first_name=self.first_name,
                       last_name=self.last_name,
                       password=self.password,
                       age=self.age,
                       address=self.address,
                       sex=self.sex,
                       history=self.history,
                       contact=self.contact,
                       salary=salary
                       )
        emp.save(con)
        return emp

    def make_doctor(self, con: Connection, salary: int = 0, room: Room = Room('null'), ):
        self.delete(con)
        doc = Doctor(username=self.id,
                     first_name=self.first_name,
                     last_name=self.last_name,
                     password=self.password,
                     age=self.age,
                     address=self.address,
                     sex=self.sex,
                     history=self.history,
                     contact=self.contact,
                     salary=salary,
                     room=room,
                     )
        doc.save(con)
        return doc

    def make_receptionist(self, con: Connection, salary: int = 0):
        self.delete(con)
        rec = Receptionist(username=self.id,
                           first_name=self.first_name,
                           last_name=self.last_name,
                           password=self.password,
                           age=self.age,
                           address=self.address,
                           sex=self.sex,
                           history=self.history,
                           contact=self.contact,
                           salary=salary,
                           )
        rec.save(con)
        return rec


class Patient(User):
    def __init__(self,
                 username: str,
                 first_name: str = '',
                 last_name: str = '',
                 password: str = '',
                 age: int = 0,
                 sex: str = 'Male',
                 address: str = '',
                 history: str = '',
                 contact: str = '',
                 room: Room = Room('null')):
        super().__init__(username=username,
                         first_name=first_name,
                         last_name=last_name,
                         password=password,
                         age=age,
                         sex=sex,
                         address=address,
                         history=history,
                         contact=contact
                         )
        self.room = ForeignKey(room.key)

    def get_records(self, con):
        records = []
        for r in con.client.smembers('one_to_many:' + self.key + ':' + Appointment.__name__.lower()):
            record = Appointment.get_by_key(r, con)
            if record is not None:
                records.append(record)
        return records

    def make_doctor(self, con: Connection, salary: int = 0, room: Room = Room('null'), ):
        self.delete(con)
        doc = Doctor(username=self.id,
                     first_name=self.first_name,
                     last_name=self.last_name,
                     password=self.password,
                     age=self.age,
                     address=self.address,
                     sex=self.sex,
                     history=self.history,
                     contact=self.contact,
                     salary=salary,
                     room=room,
                     )
        doc.save(con)
        return doc

    def make_receptionist(self, con: Connection, salary: int = 0):
        self.delete(con)
        rec = Receptionist(username=self.id,
                           first_name=self.first_name,
                           last_name=self.last_name,
                           password=self.password,
                           age=self.age,
                           address=self.address,
                           sex=self.sex,
                           history=self.history,
                           contact=self.contact,
                           salary=salary,
                           )
        rec.save(con)
        return rec

class Employee(User):
    def __init__(self,
                 username: str,
                 first_name: str = '',
                 last_name: str = '',
                 password: str = '',
                 age: int = 0,
                 sex: str = 'Male',
                 address: str = '',
                 history: str = '',
                 contact: str = '',
                 salary: int = 0):
        super().__init__(username=username,
                         first_name=first_name,
                         last_name=last_name,
                         password=password,
                         age=age,
                         address=address,
                         sex=sex,
                         history=history,
                         contact=contact)
        self.salary = salary


class Receptionist(Employee):
    pass


class Nurse(Employee):
    def get_rooms(self, con: Connection):
        rooms = []
        keys = con.client.keys('nursegovernsroom:' + self.id + '_*')
        for key in keys:
            m2m = NurseGovernsRoom.get_by_key(key, con)
            rooms.append(Room.get_by_key(m2m.room_key, con))
        return rooms

    def add_room(self, desc: str, room: Room, con: Connection):
        m2m = NurseGovernsRoom(desc, nurse=self, room=room)
        # False if already exist
        return m2m.save(con)

    def del_room(self, room: Room, con: Connection):
        m2m = NurseGovernsRoom.get_by_id(self.id + '_' + room.id, con)
        # False if relation not exist
        return m2m.delete(con)


class NurseGovernsRoom(DBEntity):
    def __init__(self,
                 description: str = '',
                 nurse: Nurse = Nurse('null'),
                 room: Room = Room('null')):
        super().__init__(dbid=nurse.id + '_' + room.id)
        self.description = description
        self.nurse_key = nurse.key
        self.room_key = room.key


class Doctor(Employee):
    def __init__(self,
                 username: str,
                 first_name: str = '',
                 last_name: str = '',
                 password: str = '',
                 age: int = 0,
                 sex: str = 'Male',
                 address: str = '',
                 history: str = '',
                 contact: str = '',
                 salary: int = 0,
                 room: Room = Room('null')):
        super().__init__(username=username,
                         first_name=first_name,
                         last_name=last_name,
                         password=password,
                         age=age,
                         address=address,
                         sex=sex,
                         history=history,
                         contact=contact,
                         salary=salary)
        self.room = ForeignKey(room.key)

    def get_appointments(self, con):
        records = []
        for r in con.client.smembers('one_to_many:' + self.key + ':' + Appointment.__name__.lower()):
            record = Appointment.get_by_key(r, con)
            if record is not None:
                records.append(record)
        return records


class Appointment(DBEntity):
    def __init__(self,
                 description: str = '',
                 patient: Patient = Patient('null'),
                 doctor: Doctor = Doctor('null'),
                 date: str = str(datetime.now().date()),
                 time: str = '',
                 time_slot: int = 0,
                 creation_date: str = '',
                 ):
        super().__init__(date + '_' + str(time) + '_' + patient.id + '_' + doctor.id)
        self.description = description
        self.patient = ForeignKey(patient.key)
        self.doctor = ForeignKey(doctor.key)
        self.time_slot = time_slot
        self.date = date
        self.creation_date = creation_date
        self.time = str(time_class(9 + time_slot // 2, 30 * (time_slot % 2)).strftime("%H:%M"))

    def get_patient(self, con):
        # None if deleted
        return Appointment.get_by_key(self.patient.key, con)
