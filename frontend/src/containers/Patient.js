import React, { Component } from 'react';
import './Patient.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import './react-notifications.css'

class AddAppointment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            doctor: null,
            patient: null,
            date: null,
            time: null,
            doctors: null,
            patients: null
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.loadDoctors();

        if (this.props.t.type === 'receptionist') {
          this.loadPatients();
          console.log("rep")
        }
    }

    loadDoctors() {
        fetch(`${process.env.REACT_APP_BACKEND_URL}/doctors/`, {
            headers: {
                'Authorization': `token ${localStorage.getItem('token')}`
            }
        })
          .then(d => d.json())
          .then(doctors => {
              this.setState({
                  doctors
              })
          });
    }

    loadPatients() {
      fetch(`${process.env.REACT_APP_BACKEND_URL}/patients/`, {
        headers: {
          'Authorization': `token ${localStorage.getItem('token')}`
        }
      })
        .then(d => d.json())
        .then(patients => {
          this.setState({
            patients
          })
        });
    }

    handleChange(e, t) {
        let s = this.state;
        if (t === 'time_hrs') {
          s.time = +e.target.value - 9;
        } else if (t === 'time_min') {
          s.time = !!+e.target.value ? 1 : 0;
        }
        s[t] = e.target.value;
        this.setState(s);
    }

    render() {
        let {doctor, patient, date, time, doctors, patients} = this.state;
        let m = {"01": 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr', '05': 'May',
          '06': "Jun", '07': 'Jul', '08': 'Aug', '09': 'Sep', "10": 'Oct', "11": 'Nov', "12": "Dec"};
        return (
          <form className="addAppointment" onSubmit={this.props.createAppointment}>
              <label>
                  Doctor:
                  <select name='doctor'>
                      {doctors ? Object.keys(doctors).map(d => <option value={d}>{doctors[d]}</option>) : null}
                  </select>
                  {/*<input type="text" value={doctor} onChange={(e) => this.handleChange(e, 'doctor')} />*/}
              </label>
              <label>
                  Patient:
                  {this.props.t.type === 'receptionist'
                    ? <select name='patient'>
                        {patients ? Object.keys(patients).sort().map(d => <option value={d}>{d}</option>) : null}
                      </select>
                    : <input type="text" disabled={true} name='patient' value={this.props.patient} onChange={(e) => this.handleChange(e, 'patient')} />}
              </label>
              <label>
                  Date:
                  {/*<input type="text" name='date' value={date} onChange={(e) => this.handleChange(e, 'date')} />*/}
                  <select name='d_y' onChange={(e) => this.handleChange(e, 'd_y')}>
                    {["2019", '2020', '2021'].map(d => <option value={d} >{d}</option>)}
                  </select>
                  <select name='d_m' onChange={(e) => this.handleChange(e, 'd_m')}>
                    {Object.keys(m).map(d => <option value={d} >{m[d]}</option>)}
                  </select>
                  <select name='d_d' onChange={(e) => this.handleChange(e, 'd_d')}>
                    {["01", '02', '03', '04', '05', '06', '07', '08', '09', 10, 11, 12, 13, 14, 15, 16,
                      17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28]
                      .map(d => <option value={d} >{d}</option>)}
                  </select>
              </label>
              <label>
                  Time:
                  {/*<input type="text" name='time' value={time} onChange={(e) => this.handleChange(e, 'time')} />*/}
                  <select name='time_hrs' onChange={(e) => this.handleChange(e, 'time_hrs')}>
                    {['09', 10, 11, 12, 13, 14, 15, 16, 17, 18].map(d => <option value={+d} >{d}</option>)}
                  </select>
                  <select name='time_min' onChange={(e) => this.handleChange(e, 'time_min')}>
                    {["00", '30'].map(d => <option value={+d} >{d}</option>)}
                  </select>
              </label>
              <input type="submit" value="Submit" />
        </form>
        )
    }
}

class Patient extends Component {
    constructor(props) {
        super(props);

        this.state = {
            adding: false,
            profile: null,
            appointments: null
        };
    }

  createNotification = (type, msg) => {
    return () => {
      switch (type) {
        case 'Error':
          NotificationManager.error(msg, type);
          break;
        case 'Success':
          NotificationManager.success(msg, type);
          break;
      }
    };
  };

    componentDidMount() {
        let h = {
            headers: {
                'Authorization': `token ${localStorage.getItem('token')}`
            }
        };
        fetch(`${process.env.REACT_APP_BACKEND_URL}/profile/`, h)
          .then(d => d.json())
          .then(profile => {
              this.setState({
                  profile
              })
          });
        fetch(`${process.env.REACT_APP_BACKEND_URL}/appointments/`, h)
          .then(d => d.json())
          .then(appointments => {
              this.setState({
                  appointments
              })
          });
    }

    toggleAddAppointment() {
        if (!this.state.adding) {
            this.setState({
                adding: !this.state.adding
            });
        }
    }

    onAppointmentCreate(e) {
        e.preventDefault();

        let o = {};
        let formData = new FormData(e.target);
        formData.forEach((value, key) => {o[key] = value});
        // o.time = Number(o.time);
        o.date = `${o.d_y}-${o.d_m}-${o.d_d}`;
        o.time_slot = (+o.time_hrs - 9) * 2 + (!!+o.time_min ? 1 : 0);
        let json = JSON.stringify(o);

        fetch(`${process.env.REACT_APP_BACKEND_URL}/appointments/`, {
            method: 'POST',
            headers: {
                'Authorization': `token ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: json
        })
          .then(d => d.json())
          .then(res => {
              if (res === 'Appointment created') {
                  this.createNotification('Success', 'Appointment created')();
                  fetch(`${process.env.REACT_APP_BACKEND_URL}/appointments/`,{
                    headers: {
                      'Authorization': `token ${localStorage.getItem('token')}`
                  }})
                    .then(d => d.json())
                    .then(appointments => {
                        this.setState({
                            appointments,
                            adding: false
                        })
                    });
              } else {
                console.log(res);
                this.createNotification('Error', res.Error)();
              }
          });
    }

    render() {
        let {profile, appointments, adding} = this.state;
        let form = <b>+</b>;

        if (adding) {
            form = <AddAppointment t={this.state.profile} patient={this.state.profile.username} createAppointment={this.onAppointmentCreate.bind(this)} />
        }

        return (
          <div style={{padding: '0 50px'}}>
              <NotificationContainer/>
              <div>
                  <h3>User:</h3>
                  {profile ? Object.keys(profile)
                    .map(d => <div><b>{d}</b>: {profile[d]}</div>) : null}
              </div>
              <div style={{marginTop: '30px'}}>
                  <h3>Appointments:</h3>
                  <div id="add_appointment" className="appointment" onClick={this.toggleAddAppointment.bind(this)}>
                      {form}
                  </div>
                  {appointments ? appointments.map(a => <div className='appointment'>{Object.keys(a).map(d => <div><b>{d}</b>: {a[d]}</div>)}</div>) : null}
              </div>
          </div>
        );
    }
}

export default Patient;
