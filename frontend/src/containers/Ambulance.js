import React from "react";
import { YMaps, Map, GeolocationControl, GeoObject} from 'react-yandex-maps';

class Ambulance extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      lat: null,
      long: null,
      points: null,
      my_lat: 0,
      my_long: 0,
      geolocationEnabled: false
    };
  }
  map = null;

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(position => {

      let s = this.state;
      s.my_long = position.coords.longitude;
      s.my_lat = position.coords.latitude;
      s.geolocationEnabled = true;
      this.setState(s);

      fetch(`${process.env.REACT_APP_BACKEND_URL}/ambulances/?lat=${s.my_lat}&long=${s.my_long}`, {
        method: 'POST',
        headers: {
          'Authorization': `token ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      })
        .then(d => d.json())
        .then(points => {
          let slong = points.reduce((d, e) => d + e.long, s.long);
          let slat = points.reduce((d, e) => d + e.lat, s.lat);
          let c = points.length;

          this.setState({
            points,
            long: slong/c,
            lat: slat/c
          })
        });
    }, () => {
      this.setState({
        geolocationEnabled: false
      })
    });
  }

  handleApiAvailable = ymaps => {
    console.log(".");
    ymaps
      .route(
        [
          { type: "wayPoint", point: [this.state.my_lat, this.state.my_long] },
          { type: "wayPoint", point: [this.state.points ? this.state.points[0].lat : this.state.my_lat, this.state.points ? this.state.points[0].long : this.state.my_long] }
        ],
        {
          mapStateAutoApply: true
        }
      )
      .then(route => {
        route.getPaths().options.set({
          // в балуне выводим только информацию о времени движения с учетом пробок
          balloonContentBodyLayout: ymaps.templateLayoutFactory.createClass(
            "$[properties.humanJamsTime]"
          ),
          // можно выставить настройки графики маршруту
          strokeColor: "0000ffff",
          opacity: 0.9
        });

        // добавляем маршрут на карту
        this.map.geoObjects.add(route);
      });
  };

  render() {
    let {my_long, my_lat} = this.state;

    let ya_map =
      <Map height="100%" width="100%"
           defaultState={{ center: [my_lat, my_long], zoom: 11 }}
           instanceRef={ref => (this.map = ref)}>
        <GeolocationControl options={{ float: 'left' }} />
        <GeoObject
          geometry={{
            type: 'Point',
            coordinates: [my_lat, my_long],
          }}
          options={{
            preset: "islands#redDotIcon"
          }}
        />
        {this.state.points ? this.state.points.map(d => <GeoObject
          geometry={{
            type: 'Point',
            coordinates: [d.lat, d.long],
          }}
          options={{
            iconColor: '#3caa3c',
            preset: 'islands#circleIcon',
          }}
          properties={{
            iconContent: '<b style="color: #3caa3c;">A</b>',
          }}
        />) : null}
      </Map>;

    return (
      <YMaps style={{height: '100%', width: '100%'}} query={{lang: "en_RU"}}
             onApiAvaliable={ymaps => this.handleApiAvailable(ymaps)}>
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%'}}>
          {this.state.geolocationEnabled ? ya_map : "Please enable geolocation in browser to view the map :)"}
        </div>
      </YMaps>
    );
  }
}

export default Ambulance;
