import {
    Form, Input, Select, Checkbox, Button, AutoComplete} from 'antd';
import { connect } from 'react-redux';
import React from "react";
import * as actions from '../store/actions/auth';
import { Icon } from 'antd';
import axios from "axios";
import {authSuccess} from "../store/actions/auth";
import {checkAuthTimeout} from "../store/actions/auth";
import {authFail} from "../store/actions/auth";

const { Option } = Select;
const AutoCompleteOption = AutoComplete.Option;

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class HorizontalLoginForm extends React.Component {
    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
              axios.post(`${process.env.REACT_APP_BACKEND_URL}/login/`, {
                username: values.userName,
                password: values.password
              })
                .then(res => {
                  const token = res.data.token;
                  const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
                  localStorage.setItem('token', token);
                  localStorage.setItem('user', values.userName);
                  localStorage.setItem('expirationDate', expirationDate);
                  this.props.history.push('/profile');
                })
                .catch(err => {
                  console.log(err)
                })
            }
        });
    }

    render() {
        const {
            getFieldDecorator, getFieldsError, getFieldError, isFieldTouched,
        } = this.props.form;

        // Only show error after a field is touched.
        const userNameError = isFieldTouched('userName') && getFieldError('userName');
        const passwordError = isFieldTouched('password') && getFieldError('password');
        return (
            <div className='regForm'>
                <div style={{textAlign: "center", margin:'3%'}}>
                    Log In
                </div>
                <div>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Item
                            validateStatus={userNameError ? 'error' : ''}
                            help={userNameError || ''}
                        >
                            {getFieldDecorator('userName', {
                                rules: [{ required: true, message: 'Please input your username!' }],
                            })(
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                            )}
                        </Form.Item>
                        <Form.Item
                            validateStatus={passwordError ? 'error' : ''}
                            help={passwordError || ''}
                        >
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Button
                                type="primary"
                                htmlType="submit"
                                disabled={hasErrors(getFieldsError())}
                            >
                                Log in
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        );
    }
}

const LogIn = Form.create({ name: 'horizontal_login' })(HorizontalLoginForm);

const mapStateToProps = (state) => {
  return {
    loading: state.loading,
    error: state.error,
    isAuthenticated: state.token !== null
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (username, password) => dispatch(actions.authLogin(username, password))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogIn);
