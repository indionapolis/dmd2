import React from "react";
import {Route} from "react-router-dom";
import SignUp from "./containers/SignUp";
import WelcomePage from "./containers/WelcomePage";
import LogIn from "./containers/LogIn";
import Patient from "./containers/Patient";
import Ambulance from "./containers/Ambulance";


const BaseRouter = () => (
    <div style={{height: '100%'}}>
        <Route exact path='/' component={WelcomePage} />
        <Route exact path='/signup' component={SignUp} />
        <Route exact path='/login' component={LogIn} />
        <Route exact path='/profile' component={Patient} />
        <Route exact path='/ambulance' component={Ambulance} />
    </div>
);

export default BaseRouter;
