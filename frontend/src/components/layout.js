import {Layout, Menu, Breadcrumb, Card} from 'antd';
import {Component} from "react";
import React from "react";
import { withRouter, Link} from 'react-router-dom';
import './s.css';

const { Header, Content, Footer } = Layout;

class CustomLayout extends Component {
    render() {

        return (
            <Layout className="layout" style={{height: '100%'}}>
                <Header>
                    <div className="logo">
                        Innopolis Medical Centre
                    </div>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['3']}
                        style={{ lineHeight: '64px', float:'right' }}
                    >
                      <Menu.Item key="menu-1" onClick={()=>{this.props.history.push('/');}}>About</Menu.Item>
                      <Menu.Item key="menu0" onClick={()=>{this.props.history.push('/profile');}} className={!localStorage.getItem('token') ? 'unused_menu' : ''}>Profile</Menu.Item>
                      <Menu.Item key="menu1" onClick={()=>{this.props.history.push('/login/');}} className={localStorage.getItem('token') ? 'unused_menu' : ''}>Login</Menu.Item>
                      <Menu.Item key="menu2" onClick={()=>{this.props.history.push('/signup/');}} className={localStorage.getItem('token') ? 'unused_menu' : ''}>Register</Menu.Item>
                      <Menu.Item key="menu3" onClick={()=>{localStorage.removeItem('token'); this.props.history.push('/login/');}} className={!localStorage.getItem('token') ? 'unused_menu' : ''}>Logout</Menu.Item>
                      <Menu.Item key="menu4" onClick={()=>{this.props.history.push('/ambulance');}}>CALL AMBULANCE</Menu.Item>
                    </Menu>

                </Header>
                {/*<img src={image1} style={{width:"100%", position:"absolute", zIndex:0, margin:"64px 0 0 0"}}/>*/}

                <Content style={{ padding: '0', background:"white", zIndex:1, height: '100%'}}>
                    {this.props.children}
                </Content>
            </Layout>
        );
    }
}

export default withRouter(CustomLayout);
