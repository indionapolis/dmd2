from datetime import datetime, timedelta
import random
import string
import csv
import sys
sys.path.append('..')

from backend.database.models import *
from backend.database.DBApi import Connection
from backend.database.geomodels import *


def get_list(func, n):
    s = set()
    while len(s) < n:
        s |= {func()}
    return s


def get_login():
    fnames = ['Aaron', 'Adam', 'Alexander', 'Amanda', 'Amy', 'Andrew', 'Angela', 'Anna', 'Anthony', 'Ashley', 'Barbara',
              'Benjamin', 'Betty', 'Brandon', 'Brenda', 'Brian', 'Carol', 'Charles', 'Christina', 'Christine',
              'Christopher', 'Cynthia', 'Daniel', 'David', 'Deborah', 'Debra', 'Dennis', 'Donald', 'Donna', 'Edward',
              'Elizabeth', 'Emily', 'Eric', 'Gary', 'George', 'Gregory', 'Heather', 'Jacob', 'James', 'Jason',
              'Jeffrey', 'Jennifer', 'Jessica', 'John', 'Jonathan', 'Jordan', 'Jose', 'Joseph', 'Joshua', 'Julie',
              'Justin', 'Karen', 'Katherine', 'Kathleen', 'Kelly', 'Kenneth', 'Kevin', 'Kimberly', 'Kyle', 'Larry',
              'Laura', 'Linda', 'Lisa', 'Margaret', 'Maria', 'Mark', 'Mary', 'Matthew', 'Melissa', 'Michael',
              'Michelle', 'Nancy', 'Nathan', 'Nicholas', 'Nicole', 'Pamela', 'Patricia', 'Patrick', 'Paul', 'Rachel',
              'Rebecca', 'Richard', 'Robert', 'Ronald', 'Ryan', 'Samantha', 'Samuel', 'Sandra', 'Sarah', 'Scott',
              'Sharon', 'Stephanie', 'Stephen', 'Steven', 'Susan', 'Thomas', 'Timothy', 'Tyler', 'William', 'Zachary']
    snames = ['ADAMS', 'ALLEN', 'ALVAREZ', 'ANDERSON', 'BAILEY', 'BAKER', 'BELL', 'BENNETT', 'BROOKS', 'BROWN',
              'CAMPBELL', 'CARTER', 'CASTILLO', 'CHAVEZ', 'CLARK', 'COLLINS', 'COOK', 'COOPER', 'COX', 'CRUZ', 'DAVIS',
              'DIAZ', 'EDWARDS', 'EVANS', 'FLORES', 'FOSTER', 'GARCIA', 'GOMEZ', 'GONZALES', 'GONZALEZ', 'GRAY',
              'GREEN', 'GUTIERREZ', 'HALL', 'HARRIS', 'HERNANDEZ', 'HILL', 'HOWARD', 'HUGHES', 'JACKSON', 'JAMES',
              'JIMENEZ', 'JOHNSON', 'JONES', 'KELLY', 'KIM', 'KING', 'LEE', 'LEWIS', 'LONG', 'LOPEZ', 'MARTIN',
              'MARTINEZ', 'MENDOZA', 'MILLER', 'MITCHELL', 'MOORE', 'MORALES', 'MORGAN', 'MORRIS', 'MURPHY', 'NELSON',
              'NGUYEN', 'ORTIZ', 'PARKER', 'PEREZ', 'PETERSON', 'PHILLIPS', 'PRICE', 'RAMIREZ', 'RAMOS', 'REED',
              'REYES', 'RICHARDSON', 'RIVERA', 'ROBERTS', 'ROBINSON', 'RODRIGUEZ', 'ROGERS', 'RUIZ', 'SANCHEZ',
              'SANDERS', 'SCOTT', 'SMITH', 'STEWART', 'TAYLOR', 'THOMAS', 'THOMPSON', 'TORRES', 'TURNER', 'VASQUEZ',
              'WALKER', 'WARD', 'WATSON', 'WHITE', 'WILLIAMS', 'WILSON', 'WOOD', 'WRIGHT', 'YOUNG']
    return random.choice(fnames).lower() + '_' + random.choice(snames).lower()


def get_password():
    return ''.join(map(lambda x: random.choice(string.ascii_letters + string.digits), range(12)))


def get_address():
    return f'{55.2517 + random.random()} N, {48.2473 + random.random()} E'


def get_contact():
    f = lambda x: ''.join(map(lambda _: str(random.randint(0, 9)), range(x)))
    return f'+7 ({f(3)}) {f(3)}-{f(2)}-{f(2)}'


def get_date():
    return str(datetime.today().date() + timedelta(days=random.randint(1, 8)))


def get_drug():
    return random.choice(
        ['Abacavir Sulfate', 'Abatacept', 'Abilify', 'Acamprosate Calcium', 'Accretropin', 'Aceon', 'Aci-Jel',
         'Acthrel', 'Actimmune', 'Actisite', 'Acular', 'Acular LS', 'Acuvail', 'Adagen', 'Adapalene', 'Adcirca',
         'Adefovir Dipivoxil', 'Adenoscan', 'Adenosine', 'Adipex-P', 'AdreView', 'Advair HFA', 'Aerospan HFA',
         'Agalsidase Beta', 'Aggrenox', 'Akineton', 'Alamast', 'Albenza', 'Aldactazide', 'Aldactone', 'Aldoril',
         'Aldurazyme', 'Alemtuzumab', 'Alglucosidase Alfa', 'Allegra-D', 'Allegra D 24 Hour', 'Alli', 'Aloprim',
         'Alora', 'Alphanate', 'Altace', 'Altocor', 'Altoprev', 'Alupent', 'Amantadine Hydrochloride', 'Amerge',
         'Amifostine', 'Amiloride', 'Aminosalicylic Acid', 'Aminosyn II 8.5%', 'Amlodipine Besylate', 'Amoxapine',
         'Amytal Sodium', 'Anabolic steroids', 'Anadrol-50', 'Antithrombin', 'Antivenin', 'Antivert', 'Aredia',
         'Aricept', 'Armodafinil', 'Arranon', 'Artane', 'Asclera', 'Ascorbic Acid', 'Astemizole', 'Atacand',
         'Atacand HCT', 'Atazanavir Sulfate', 'Atomoxetine HCl', 'Atridox', 'Atripla', 'Atropen', 'Augmentin XR',
         'Avage', 'Avandia', 'Avastin', 'Avinza', 'Axid', 'Azasan', 'Azasite', 'Azelaic Acid',
         'Azelastine Hydrochloride', 'Azilect', 'Azmacort', 'Balsalazide', 'Benazepril', 'Benzocaine',
         'Benzoyl Peroxide Gel', 'Benzphetamine', 'Benztropine Mesylate', 'Bepreve', 'Betagan', 'Bethanechol',
         'Betimol', 'Betoptic S', 'Bevacizumab', 'BiCNU', 'Biperiden', 'Bismuth Subcitrate Potassium',
         'Bismuth Subsalicylate', 'Blocadren', 'Boniva', 'Bontril', 'Boostrix', 'Botulinum Toxin Type A', 'Bravelle',
         'Brevibloc', 'Bromocriptine Mesylate', 'Brovana', 'Budesonide', 'Buprenorphine', 'Buspar', 'Buspirone',
         'Busulfan', 'Busulfex', 'Cabergoline', 'Caduet', 'Calcitonin-Salmon', 'Calcium Chloride',
         'Calcium Disodium Versenate', 'Calcium Gluconate', 'Campral', 'Canasa', 'Cancidas', 'Captopril', 'Carac',
         'Carbatrol', 'Cardiolite', 'Carisoprodol', 'Carmustine', 'Carvedilol', 'Casodex', 'Caspofungin Acetate',
         'Cataflam', 'Catapres', 'Catapres-TTS', 'Caverject', 'Cedax', 'Cefditoren Pivoxil', 'Cefixime', 'Cefizox',
         'Cefotetan', 'Ceftazidime', 'Ceftibuten', 'Ceftin', 'Cefzil', 'Celestone Soluspan', 'Celexa', 'CellCept',
         'Cellulose', 'Celontin', 'Cephalexin', 'Cerebyx', 'Ceretec', 'Cerubidine', 'Cerumenex', 'Cervidil',
         'Cetirizine', 'Cetraxal', 'Cetrotide', 'Cetuximab', 'Chantix', 'Chibroxin', 'Chlorambucil',
         'Chloramphenicol Sodium Succinate', 'Chloroprocaine', 'Chlorpheniramine Maleate', 'Chlorpromazine',
         'Chlorpropamide', 'Chlorthalidone', 'Cholera Vaccine', 'Chorionic Gonadotropin', 'Ciclopirox Gel',
         'Cilostazol', 'Cinobac', 'Cipro', 'Cipro XR', 'Cisapride', 'Clarinex', 'Clarithromycin', 'Claritin', 'Cleocin',
         'Cleviprex', 'Climara Pro', 'Clinoril', 'Clobetasol Propionate', 'Clocortolone', 'Clofarabine', 'Clonidine',
         'Clorazepate Dipotassium', 'Clorpres', 'Clotrimazole', 'Cocaine', 'Codeine', 'Cognex', 'Colazal', 'Colchicine',
         'Colcrys', 'Colesevelam Hcl', 'Combivir', 'Conjugated Estrogens', 'Copaxone', 'Corgard', 'Cosmegen',
         'Coumadin', 'Crolom', 'Cromolyn Sodium', 'Cubicin', 'Curosurf', 'Cuvposa', 'Cyanocobalamin',
         'Cyclobenzaprine Hcl', 'Cyclophosphamide', 'Cyclosporine', 'Cylert', 'Cymbalta', 'Cyproheptadine', 'Cystadane',
         'Cytogam', 'Cytomel', 'Dacarbazine', 'Daraprim', 'Darvocet-N', 'Darvon Compound', 'Dasatinib', 'Daunorubicin',
         'Daypro', 'Daypro Alta', 'DDAVP Nasal Spray', 'Demadex', 'Demeclocycline HCl', 'Demser', 'Depacon', 'DepoDur',
         'Desferal', 'Desogen', 'Desonate', 'DesOwen', 'Detrol', 'Detrol LA', 'Dexlansoprazole',
         'Dexmethylphenidate Hydrochloride', 'Dexrazoxane', 'Diamox Sequels', 'Dicyclomine', 'Didanosine',
         'Diethylpropion', 'Differin', 'Diflucan', 'Digoxin Immune Fab', 'Diovan HCT', 'Diphenhydramine',
         'Diphtheria-Tetanus Vaccine', 'Diprolene AF', 'Dipyridamole', 'Ditropan', 'Dobutamine', 'Dofetilide',
         'Dolophine', 'Donepezil Hydrochloride', 'Dopamine Hydrochloride', 'Dopar', 'Dopram', 'Doral', 'Doryx',
         'Dorzolamide', 'Dovonex', 'Doxacurium Chloride', 'Doxapram', 'Doxazosin Mesylate', 'Doxepin',
         'Doxercalciferol', 'Doxil', 'Doxycycline', 'Doxycycline Hyclate', 'Drisdol', 'Dronabinol',
         'Drospirenone and Estradiol', 'Duetact', 'Duraclon', 'Dynacirc', 'Dynacirc CR', 'Dynapen', 'Dyphylline',
         'Econazole Nitrate', 'Edrophonium', 'Efavirenz', 'Elaprase', 'Elavil', 'Eletriptan hydrobromide', 'Eligard',
         'Ellence', 'Elmiron', 'Elspar', 'Emadine', 'Emcyt', 'Emedastine', 'Empirin', 'Emsam', 'Emtricitabine',
         'Emtriva', 'Endocet', 'Endometrin', 'Enflurane', 'Engerix-B', 'Entereg', 'Eovist', 'Epinephrine', 'Epipen',
         'Epirubicin hydrochloride', 'Epivir', 'Equetro', 'Eraxis', 'Erbitux', 'Ergocalciferol', 'Erlotinib',
         'Erythrocin Stearate', 'Esomeprazole Sodium', 'Essential Amino Acids', 'Estrace', 'Estradiol',
         'Estradiol Acetate', 'Estradiol valerate', 'Estratest', 'Estropipate', 'Eszopiclone', 'Etanercept',
         'Ethacrynic Acid', 'Ethambutol', 'Ethinyl Estradiol', 'Ethiodol', 'Ethosuximide', 'Etidocaine HCl',
         'Etidronate Disodium', 'Etopophos', 'Etrafon', 'Eulexin', 'Evista', 'Evoxac', 'Exelderm', 'Exjade', 'Extavia',
         'Factor IX Complex', 'Factrel', 'Famciclovir', 'Famotidine Injection', 'Famvir', 'Fansidar', 'Febuxostat',
         'Feridex I.V.', 'Fesoterodine Fumarate Extended', 'Finacea', 'Flector', 'Flonase', 'Florinef', 'Floxuridine',
         'Fluconazole', 'Flucytosine', 'Fludara', 'Fludarabine Phosphate', 'Fludrocortisone', 'Flumazenil', 'FluMist',
         'Fluocinolone Acetonide', 'Fluoroplex', 'Fluorouracil', 'Fluoxetine Hydrochloride', 'Flurbiprofen', 'Fluress',
         'Fluticasone Propionate', 'Fluvirin', 'FML', 'Folic Acid', 'Follitropin Alfa', 'Follitropin Beta',
         'Fomepizole', 'Foradil Aerolizer', 'Foradil Certihaler', 'Forane', 'Fosamax Plus D', 'Fosamprenavir Calcium',
         'Foscavir', 'Fosphenytoin Sodium', 'Fragmin', 'Frovatriptan Succinate', 'Fulvestrant', 'Fungizone',
         'Furadantin', 'Furosemide', 'Furoxone', 'Fuzeon', 'Gabitril', 'Gadobenate Dimeglumine',
         'Gadofosveset Trisodium', 'Galsulfase', 'Gamunex', 'Geocillin', 'Geodon', 'Gleevec', 'Glucophage XR',
         'Glucovance', 'Glyburide', 'Glycopyrrolate', 'Glynase', 'Glyset', 'Gold Sodium Thiomalate', 'Gonadorelin',
         'Gonal-F', 'Gonal-f RFF', 'Grifulvin V', 'Griseofulvin', 'Guanethidine Monosulfate', 'Gynazole',
         'Haemophilus b Conjugate Vaccine', 'Halcinonide', 'Haldol', 'Halobetasol Propionate', 'Haloperidol', 'Healon',
         'HepaGam B', 'Heparin Lock Flush', 'HepatAmine', 'Hepatitis A Vaccine,  Inactivated',
         'Hepatitis B Immune Globulin', 'Hepflush-10', 'Herceptin', 'Hexachlorophene', 'HibTITER', 'Hivid',
         'Human Secretin', 'Humira', 'Humulin N', 'Hyalgan', 'Hydrocodone Bitartrate and Acetaminophen',
         'Hydroxyethyl Starch', 'Hylenex', 'Hyoscyamine', 'Hytrin', 'Ibuprofen Lysine', 'Idamycin', 'Idamycin PFS',
         'Ifosfamide', 'Iloperidone', 'Imipramine', 'Imiquimod', 'Imitrex', 'Immune Globulin',
         'Immune Globulin Intravenous', 'Implanon', 'Inderal LA', 'Indigo Carmine', 'InnoPran XL', 'Insulin',
         'Insulin Aspart', 'Intelence', 'Intralipid 20%', 'Intuniv', 'Invanz', 'Invega', 'Inversine', 'Ionamin',
         'Irinotecan Hydrochloride', 'Isentress', 'Ismo', 'Isocarboxazid', 'Isoptin SR', 'Isopto Carpine',
         'Isopto Hyoscine', 'Istalol', 'Isuprel', 'Ixempra', 'Jalyn', 'Janumet', 'Je-Vax', 'K-LOR', 'Kaletra', 'Kariva',
         'Kenalog', 'Kinlytic', 'Klonopin', 'Kuvan', 'Kytril', 'Labetalol', 'lacosamide', 'Lamisil',
         'Lamivudine / Zidovudine', 'Latanoprost', 'Letairis', 'Letrozole', 'Leuprolide Acetate', 'Leustatin',
         'Levalbuterol', 'Levaquin', 'Levemir', 'Levo-T', 'Levocabastine', 'Levofloxacin', 'Levonorgestrel',
         'Levonorgestrel and Ethinyl Estradiol', 'Levonorgestrel Implants', 'Levonorgestrel,  Ethinyl Estradiol',
         'Lexapro', 'Lexiscan', 'Lexxel', 'Librium', 'Lidex', 'Lidoderm', 'Linezolid', 'Lipofen', 'Liposyn II',
         'Liraglutide', 'Lisinopril and Hydrochlorothiazide', 'Locoid', 'Lodine', 'Loperamide Hcl', 'Lopid',
         'Loprox Gel', 'Loracarbef', 'Lortab', 'Lotemax', 'Lotensin', 'Lotronex', 'Lovenox', 'Loxapine', 'Loxitane',
         'Lucentis', 'Luvox CR', 'Lybrel', 'M-M-R', 'Malarone', 'Malathion', 'Mandol', 'Mangafodipir', 'Maraviroc',
         'Marinol', 'Maxitrol', 'Mecasermin', 'Meclofenamate', 'Mefloquine', 'Melphalan', 'Menactra', 'Menest',
         'Menotropins', 'Mephobarbital', 'Mequinol and Tretinoin', 'Meropenem', 'Merrem I.V.', 'Mesalamine', 'Mesna',
         'Mestinon', 'Metadate ER', 'Metaglip', 'Metaproterenol Sulfate', 'Metaxalone', 'Metformin Hcl',
         'Methadone Hydrochloride', 'Methadose Oral Concentrate', 'Methazolamide', 'Methenamine Hippurate',
         'Methergine', 'Methohexital Sodium', 'Methyclothiazide', 'Methyldopa', 'Methylene Blue',
         'Methylergonovine Maleate', 'Methylin', 'Methyltestosterone', 'Metipranolol', 'Metoclopramide',
         'Metoprolol Tartrate', 'MetroLotion', 'Metyrapone', 'Metyrosine', 'Miacalcin', 'Micro-K', 'Micronase',
         'Micronized Glyburide', 'Midazolam', 'Midodrine Hydrochloride', 'Milrinone', 'Minocin', 'Minocycline',
         'Minoxidil', 'Miochol-E', 'Miostat', 'Mitomycin', 'Mobic', 'Modafinil', 'Monistat', 'Monistat-Derm',
         'Morrhuate Sodium', 'Motrin', 'Moxatag', 'Mozobil', 'Multaq', 'Multi Vitamin', 'Multihance', 'Mustargen',
         'Mutamycin', 'Myambutol', 'Mycamine', 'Mycelex', 'Mycophenolic Acid', 'Myfortic', 'Mykrox', 'Myobloc',
         'Myochrysine', 'Nafcillin Sodium', 'Naftifine Hcl', 'Nalmefene Hydrochloride', 'Naltrexone', 'Naproxen',
         'Nascobal', 'Natazia', 'Natrecor', 'Navelbine', 'Nebcin', 'Nebivolol Tablets', 'Nedocromil', 'Nelarabine',
         'Nelfinavir Mesylate', 'NeoProfen', 'Neostigmine', 'Nephramine', 'Nesacaine', 'Neulasta', 'Nexavar', 'Niaspan',
         'Nicotrol', 'Nicotrol NS', 'Nilandron', 'Nilotinib Capsules', 'Nimbex', 'Nimotop', 'Nitroglycerin',
         'NitroMist', 'Nizatidine', 'Nizoral', 'Noctec', 'Nor-QD', 'Norethindrone and Ethinyl Estradiol', 'Noritate',
         'Nortriptyline Hydrochloride', 'Norvasc', 'NovoLog Mix 70/30', 'Novoseven', 'Numorphan', 'Nutropin AQ',
         'Nutropin Depot', 'Nydrazid', 'Omeprazole', 'Omnaris', 'Opana', 'Opticrom', 'OptiMARK', 'Optipranolol',
         'Oracea', 'Oraqix', 'Orfadin', 'Orlaam', 'Orlistat', 'Orudis', 'Ovcon', 'Ovide', 'Oxandrolone', 'Oxaprozin',
         'Oxistat', 'Oxsoralen-Ultra', 'Oxycodone HCl', 'Oxycodone Hydrochloride', 'Oxycontin', 'Oxymetholone',
         'Oxymorphone', 'Oxytetracycline', 'Paclitaxel', 'Palifermin', 'Paliperidone', 'Palonosetron hydrochloride',
         'Panhematin', 'Pantoprazole', 'Parafon Forte', 'Parnate', 'Paser', 'Pataday', 'Pazopanib', 'Pediapred',
         'PEG 3350', 'Pegfilgrastim', 'Pemirolast Potassium', 'Penciclovir', 'Penicillamine', 'Penlac',
         'Pentetate Zinc Trisodium', 'Pentobarbital', 'Pentoxifylline', 'Perflutren', 'Perindopril Erbumine', 'Permax',
         'Persantine', 'Pfizerpen', 'Phenazopyridine', 'Phenelzine', 'Phenobarbital', 'Phenoxybenzamine',
         'Phenylephrine HCl', 'Phenylephrine Hydrochloride', 'Phenytoin', 'Phosphate', 'Photofrin',
         'Pilocarpine Hydrochloride', 'Pilopine HS', 'Pindolol', 'Pipracil', 'Piroxicam', 'Plaquenil', 'PlasmaLyte A',
         'Plavix', 'Plenaxis', 'Pletal', 'Pneumovax', 'Podophyllin', 'Polidocanol', 'Polyethylene Glycol 3350',
         'Polythiazide', 'Pramipexole', 'Pred-G', 'Prednicarbate', 'Prednisolone Acetate', 'Prednisone', 'Prefest',
         'Pregnyl', 'Premarin', 'Prepidil', 'Prevpac', 'Priftin', 'Primacor', 'Primaquine', 'Primidone', 'Prinivil',
         'Prinzide', 'Pristiq', 'Procainamide', 'Procalamine', 'Prochlorperazine Maleate', 'ProHance', 'Proleukin',
         'Prolixin', 'Promethazine HCl', 'Promethazine Hydrochloride', 'Prometrium', 'Propecia', 'Proquin XR',
         'Prostin VR Pediatric', 'Protein C Concentrate', 'Protopic', 'Protriptyline Hydrochloride', 'Proventil HFA',
         'Provisc', 'Provocholine', 'Pulmicort Flexhaler', 'Pylera', 'Pyrazinamide', 'Pyridium', 'Pyridostigmine',
         'Qualaquin', 'Quazepam', 'Quinidine Sulfate', 'Quixin', 'Rabies Vaccine', 'Raltegravir', 'Ranexa',
         'Ranitidine Hcl', 'Rapamune', 'Rasagiline', 'Raxar', 'Rebetol', 'Remicade', 'Remifentanil', 'Renese', 'ReoPro',
         'Rescriptor', 'Rescula', 'Revatio', 'Revex', 'Revia', 'Reyataz', 'Rezulin', 'Rhinocort Aqua',
         'Rhogam Ultra-Filtered Plus', 'RiaSTAP', 'Rifamate', 'Riomet', 'Risperidone', 'Ritalin', 'Rituximab',
         'Rivastigmine Tartrate', 'Robinul', 'Rosiglitazone Maleate', 'Rotarix', 'RotaTeq', 'Roxicet', 'Roxicodone',
         'Ryzolt', 'Sabril', 'Sacrosidase', 'Samsca', 'Sanctura', 'Santyl', 'Saphris', 'Scopolamine', 'Seasonale',
         'Selegiline Hydrochloride', 'Selsun', 'Septra', 'Serax', 'Sertraline Hcl', 'Serzone', 'Sevoflurane',
         'Sibutramine Hydrochloride Monohydrate', 'Silenor', 'Simponi', 'Sirolimus', 'Sitagliptin Metformin HCL',
         'Slow-K', 'Sodium Bicarbonate', 'Sodium ferric gluconate', 'Sodium Iodide I 131',
         'Sodium Polystyrene Sulfonate', 'Sodium Sulfacetamide', 'Soma Compound', 'Somatrem', 'Somatropin', 'Sonata',
         'Soriatane', 'Sotradecol', 'Spiriva', 'Sporanox', 'Sprix', 'Sprycel', 'Stalevo', 'Starlix', 'Stavudine',
         'Streptokinase', 'Strontium-89', 'Suboxone', 'Succimer', 'Succinylcholine Chloride', 'Sucralfate',
         'Sulfamylon', 'Sunitinib Malate', 'Sutent', 'Synthroid', 'Synvisc', 'Syprine', 'Tacrolimus', 'Talacen',
         'Talwin Nx', 'Tamiflu', 'Tamoxifen Citrate', 'Tapazole', 'Targretin', 'Tasmar', 'Tegretol', 'Tekturna HCT',
         'Telavancin', 'Telbivudine', 'Telmisartan', 'Temovate Scalp', 'Temozolomide', 'Temsirolimus', 'Teniposide',
         'Terazol 3,  Terazol 7', 'Tessalon', 'Testolactone', 'Testred', 'Teveten HCT', 'Theracys', 'Thiabendazole',
         'Thiethylperazine', 'Thiopental Sodium', 'Thioridazine', 'Thiothixene Hcl', 'Thrombin', 'Thyrolar',
         'Thyrotropin Alfa', 'Tiazac', 'Ticarcillin', 'Tinzaparin', 'Tirosint', 'Tizanidine', 'Tobrex', 'Tofranil-PM',
         'Tolazamide', 'Tolmetin Sodium', 'Tonocard', 'Topicort', 'Topiramate', 'Topotecan Hydrochloride', 'Toradol',
         'Torsemide', 'Toviaz', 'Tramadol Hcl', 'Tranxene', 'Trastuzumab', 'Trasylol', 'Tretinoin', 'Trexall',
         'Tri-Sprintec', 'Triamcinolone Acetonide', 'Triazolam', 'Tribenzor', 'Trientine', 'Trihexyphenidyl',
         'Trilipix', 'Trilisate', 'Trimethadione', 'Trimethoprim', 'Trimethoprim and Sulfamethoxazole',
         'Trimetrexate Glucuronate', 'Trizivir', 'Trovafloxacin', 'Trovan', 'Trusopt', 'Trypan Blue', 'Tussionex',
         'Tysabri', 'Tyvaso', 'Uloric', 'Ultiva', 'Ultram', 'Ultrase', 'Ultravate', 'Unasyn', 'Urex', 'Ursodiol',
         'Vagistat-1', 'Valacyclovir Hydrochloride', 'Valganciclovir Hcl', 'Valium', 'Valproic Acid',
         'Valsartan and Hydrochlorothiazide', 'Vancomycin Hydrochloride', 'Vaprisol', 'Vasocidin', 'Vasotec',
         'Vasovist', 'Vectibix', 'Vectical', 'Velosulin', 'Veltin', 'Venlafaxine Hydrochloride', 'Veramyst', 'Vermox',
         'Vesanoid', 'VESIcare', 'Vibramycin', 'Vicodin', 'Vicodin HP', 'Vicoprofen', 'Victoza', 'Vimovo', 'Vimpat',
         'Vinblastine Sulfate', 'Viokase', 'Vioxx', 'Viread', 'VisionBlue', 'Vistide', 'Vitamin K1', 'Vivactil',
         'Vivelle-Dot', 'Vusion', 'Vytorin', 'Winstrol', 'Xigris', 'Xolair', 'Yellow Fever Vaccine', 'Zaditor',
         'Zalcitabine', 'Zanosar', 'Zelnorm', 'Zemaira', 'Zemplar', 'Zestoretic', 'Zestril', 'Ziconotide', 'Zingo',
         'Zmax', 'Zocor', 'Zolinza', 'Zolmitriptan', 'Zonalon', 'Zoster Vaccine Live', 'Zosyn', 'Zyclara', 'Zyflo',
         'Zylet', 'Zyloprim', 'Zymaxid'])


def make_users(n):
    username = get_list(get_login, n)
    first_name = list(map(lambda x: x.split('_')[0], username))
    last_name = list(map(lambda x: x.split('_')[1], username))
    age = list(map(lambda x: random.randint(18, 100), range(n)))
    gender = list(map(lambda x: random.choice(['Female', 'Male']), range(n)))
    password = get_list(get_password, n)
    address = get_list(get_address, n)
    history = ['' for _ in range(n)]
    contact = get_list(get_contact, n)
    return zip(username, first_name, last_name, password, age, gender, address, history, contact)


def make_employees(n):
    return zip(*(list(zip(*make_users(n))) + [tuple(map(lambda x: random.randint(10, 99) * 1000, range(n)))]))


def make_drugs(n):
    drugs = get_list(get_drug, n)
    number = list(map(lambda x: f'{random.randint(0, 9999):5}'.replace(' ', '0'), range(n)))
    price = list(map(lambda x: random.randint(10, 99) * 100, range(n)))
    return zip(drugs, number, price)


def write_csv(l, s):
    return
    with open(s + '.csv', 'w') as f:
        w = csv.writer(f)
        for i in l:
            w.writerow(i)


def populate_db(n):
    me = Patient(username='panddromas',
            first_name='Pavel',
            last_name='Nikulin',
            password='Inno1122',
            age=19,
            sex='Male',
            address='asdf',
            history='dasf',
            contact='dfsfd',
            room=Room('123121'))
    con = con = Connection(host='localhost',port='6379')
    con.client.flushall()
    rooms = []
    for i in range(n):
        rooms.append(Room(number=str(i)))
        rooms[-1].save(con)
    patients = []
    for p, r in zip(make_users(n), rooms):
        patients.append(Patient(*(p + (r,))))
        patients[-1].save(con)
        print(patients[-1].__dict__)

    patients[-1] = me
    doctors = []
    for d, r in zip(make_employees(n), rooms):
        doctors.append(Doctor(*(d + (r,))))
        doctors[-1].save(con)
        print(doctors[-1].__dict__)
    appointments = []
    for d, p in zip(doctors, patients):
        appointments.append(Appointment(patient=p, doctor=d, date=get_date(), time_slot=random.randint(0, 18)))
        appointments[-1].save(con)
        print(appointments[-1].__dict__)

    Receptionist('main_receptionist', 'Galy', 'Petrovich', 'Galy1122', 80, 'Female', 'dsfa', 'sdaf', 'dsf', 5).save(con)
    me.save(con)


def main():
    write_csv(make_users(50), 'patients')
    write_csv(make_employees(10), 'employees')
    write_csv(make_drugs(20), 'drugs')


def add_ambulances(n):
    con = Connection(host='localhost',port='6379')
    ambulances = Ambulances('ambulances')
    for i in range(n):
        ambulances.add(name=f'{i}',
                       lat=55.748325 + (random.random() - 0.5)*15,
                       long=48.742432 + (random.random() - 0.5)*23,
                       con=con)


if __name__ == '__main__':
    populate_db(1500)
    add_ambulances(10000)
