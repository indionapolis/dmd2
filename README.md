# Hospital Management System

Group: BS-17-8

Team: Nikulin Pavel, George Poputnikov, Rinat Baichev, Trushin Alexander, Lilya Gabdrakhimova, Sergey Bogdanik

Topic: Hospital Management System (Stakeholder: Nikita)

## Git flow for this project

1. If you about to implement new feature you should create new branch from **dev** branch and call it fearure/[name of feature].
2. Then implement your feature with unit test independent of other developers.
3. If you think that you have **implemented** your feature and you sufficintly **tested** it you can initialize merge request of your branch into **dev** branch.
4. Ping owner of repo about merge request.


## In this repository

```
├── README.md
├── backend - folder which contains Django backend and redis DBAPI
├── dataset - folder with script for auto generator for dataset
├── docker-compose.yml - config file for docker-compose
├── frontend - folder which contains React frontend application implementation
└── swagger - documentation for REST API
```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites for quick start

What things you need to run the software

```angular2html
docker-compose
```
#### Run project with docker

1. run ```docker-compose build``` in the root of the project
2. run ```docker-compose up``` to start project

By default docker-compose use ```backend endpoint``` on innopolis virtual machine (to use preloaded dataset). If you would like to launch application fully on your machine change endpoint argument for ```backend endpoint``` in ```docker-compose.yml``` and after ```docker-compose up``` run ```python /dataset/make.py``` to populate database.

ATTENTION:
```frontend will be runing on port 80 of target machine. Backend will be running on port 8000 of target machine. Redis data base runs inside docker-compos network on port 6379 under hostname "redis" thus wisible ONLY! for backend since they are inside one docker-compose network.```


### Prerequisites for dev start

What things you need to run the software
```angular2html
python 3.6
node 10.*
redis 5.*
```
#### Installing packages

```angular2html
python -> pip install -r requirements.txt (/backend)
node -> npm install (/frontend)
```
#### Run dev project

1. ```redis-server``` - database localhost:6379
2. ```python manage.py runserver``` (/backend) - backend localhost:8000
3. ```npm start``` (/frontend) - fronted localhost:3000


## Running the tests

You can run tests for backend REST API or tests for DB API. Make sure you have python's unittest module.
```angular2html
python -m unittest tests.py (/backend/Clinic)
python -m unittest login_tests.py (/backend/database)
python -m unittest models_tests.py (/backend/database)
``` 

## Built With

* [Redis](https://redis.io) - Redis is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker.
* [Django](https://www.djangoproject.com) - Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design.
* [React](https://reactjs.org) - A JavaScript library for building user.

## Project report

### Content
* [Preamble](#preamble)
* [Technology](#technology)
* [Models](#models)
* [Geospatial search](#geospatial-search)
* [Dataset generation](#dataset-generation)
* [Server Part](#server-part)
* [Frontend Part](#frontend-part)
* [GUI](#gui)

### Preamble

This project was developed as a part of course of ADMD. The project developed as Web application with REST API. The project implements simple clinic management system with ability to create different accounts, create appointments to doctor and ability to search for nearest ambulance near the patient. 

### Technology

For this project we decided to use Redis - an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker. It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyperloglogs, geospatial indexes with radius queries and streams.

### Models

![Models](src/models.png)

ORM - Object-Relational Mapping (Object-oriented approach to work with database, (for example Django framework))
CRUD (create, read, update, delete) operations: save(), get(), redefine(), delete()
Design decisions:
Python allows us easily work with class fields (as dictionaries). So we think that it is very convenient to utilize redis python library (which also represent hash data as dictionaries). DB Api is implemented using this python ability. It is very helpful to use @classmethods in parent getter method.

![ORM](src/orm.png)

#### Problems:
That was hard to implement different type of users (with inheritance), and save uniqueness of keys (for example <Doctor:10> and <Nurse:10> should not have one id, so that was complex to keep uniqueness, of different models. ).
The major problem in project database api was implementation of Relations between models. That was not enough just to store foreign-keys in hash field, because for accessing other entities, we need to check all hashes of one type and check all foreign keys of other objects (for example find all doctor’s Appointments if we have only doctor ID). So we decided to create something like indexing using REDIS sets. You can find detailed description in docstrings within a project code.

### Geospatial search

Our task was to implement geospatial search for ambulances using their latitude and longitude. Fortunately REDIS has internal implementation of Geo spatial search exactly which we need. Redis implementation use sorted set of (longitude, latitude, name) objects and searches in all such objects by radius and coordinates

![map1](src/map1.png)
![map2](src/map2.png)

### Dataset generation

All data inside database automaticali generated by python script (/dataset/make.py). There are more then 10000 random entities.

### Server Part
Server part was implemented using django-rest framework, so all communication with frontend goes through http requests/responses with data in json format. Currently there are several endpoints to deal with the system: for signin/signup, for profile view, for getting/creating appointments, and calling ambulance (method which returns coordinates of nearest ambulances around the patient)

### Frontend Part
We decided to use React for the frontend development. Welcome page includes the information about the organization e.g. name of the organization, address etc. Also this page has the menu buttons “Register” and “LogIn”. When the user finished the registration he could see the profile with all appointments immediately. In addition, a user could call ambulance (using the map) without any registration.

### GUI

#### About page
![GUI](src/gui1.png)

#### Profile page
![GUI](src/gui2.png)